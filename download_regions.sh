# These were coped from the output of "get region list" above on 19-Apr-2020
./download_region.sh "North and Central America" "North_and_Central_America"

#./download_region.sh "USA" "North_and_Central_America/USA"
#./download_region.sh "Canada" "North_and_Central_America/Canada"
#./download_region.sh "Mexico" "North_and_Central_America/Mexico"
#./download_region.sh "Cayman Islands" "North_and_Central_America/Cayman_Islands"
#./download_region.sh "Bahamas" "North_and_Central_America/Bahamas"
#./download_region.sh "Virgin Islands" "North_and_Central_America/Virgin_Islands"
#./download_region.sh "El Salvador" "North_and_Central_America/El_Salvador"
#./download_region.sh "Costa Rica" "North_and_Central_America/Costa_Rica"
#./download_region.sh "Dominican Republic" "North_and_Central_America/Dominican_Republic"
#./download_region.sh "Jamaica" "North_and_Central_America/Jamaica"
#./download_region.sh "Honduras" "North_and_Central_America/Honduras"
#./download_region.sh "Panama" "North_and_Central_America/Panama"
#./download_region.sh "Aruba" "North_and_Central_America/Aruba"
#./download_region.sh "Bermuda" "North_and_Central_America/Bermuda"
#./download_region.sh "Trinidad and Tobago" "North_and_Central_America/Trinidad_and_Tobago"
#./download_region.sh "Guatemala" "North_and_Central_America/Guatemala"
#./download_region.sh "Cuba" "North_and_Central_America/Cuba"
#./download_region.sh "Belize" "North_and_Central_America/Belize"
#./download_region.sh "Nicaragua" "North_and_Central_America/Nicaragua"
#./download_region.sh "Saint Kitts and Nevis" "North_and_Central_America/Saint_Kitts_and_Nevis"
#./download_region.sh "Saint Vincent and the Grenadines" "North_and_Central_America/Saint_Vincent_and_the_Grenadines"

./download_region.sh "South America" "South_America"

#./download_region.sh "Brazil" "South_America/Brazil"
#./download_region.sh "Argentina" "South_America/Argentina"
#./download_region.sh "Chile" "South_America/Chile"
#./download_region.sh "Venezuela" "South_America/Venezuela"
#./download_region.sh "Colombia" "South_America/Colombia"
#./download_region.sh "Peru" "South_America/Peru"
#./download_region.sh "Ecuador" "South_America/Ecuador"
#./download_region.sh "Uruguay" "South_America/Uruguay"
#./download_region.sh "Bolivia" "South_America/Bolivia"
#./download_region.sh "Paraguay" "South_America/Paraguay"
#./download_region.sh "Guyana" "South_America/Guyana"
#./download_region.sh "Suriname" "South_America/Suriname"

#./download_region.sh "Europe" "Europe"
./download_europe_regions.sh

#./download_region.sh "Germany" "Europe/Germany"
#./download_region.sh "France" "Europe/France"
#./download_region.sh "Spain" "Europe/Spain"
#./download_region.sh "Italy" "Europe/Italy"
#./download_region.sh "Portugal" "Europe/Portugal"
#./download_region.sh "Gibraltar" "Europe/Gibraltar"
#./download_region.sh "Andorra" "Europe/Andorra"
#./download_region.sh "Monaco" "Europe/Monaco"
#./download_region.sh "Vatican City" "Europe/Vatican_City"
#./download_region.sh "San Marino" "Europe/San_Marino"
#./download_region.sh "United Kingdom" "Europe/United_Kingdom"
#./download_region.sh "Ireland" "Europe/Ireland"
#./download_region.sh "Netherlands" "Europe/Netherlands"
#./download_region.sh "Belgium" "Europe/Belgium"
#./download_region.sh "Luxembourg" "Europe/Luxembourg"
#./download_region.sh "Austria" "Europe/Austria"
#./download_region.sh "Switzerland" "Europe/Switzerland"
#./download_region.sh "Liechtenstein" "Europe/Liechtenstein"
#./download_region.sh "Denmark" "Europe/Denmark"
#./download_region.sh "Norway" "Europe/Norway"
#./download_region.sh "Sweden" "Europe/Sweden"
#./download_region.sh "Finland" "Europe/Finland"
#./download_region.sh "Turkey" "Europe/Turkey"
#./download_region.sh "Greece" "Europe/Greece"
#./download_region.sh "Poland" "Europe/Poland"
#./download_region.sh "Czechia" "Europe/Czechia"
#./download_region.sh "Slovakia" "Europe/Slovakia"
#./download_region.sh "Hungary" "Europe/Hungary"
#./download_region.sh "Lithuania" "Europe/Lithuania"
#./download_region.sh "Latvia" "Europe/Latvia"
#./download_region.sh "Estonia" "Europe/Estonia"
#./download_region.sh "Ukraine" "Europe/Ukraine"
#./download_region.sh "Moldova" "Europe/Moldova"
#./download_region.sh "Belarus" "Europe/Belarus"
#./download_region.sh "Romania" "Europe/Romania"
#./download_region.sh "Bulgaria" "Europe/Bulgaria"
#./download_region.sh "Malta" "Europe/Malta"
#./download_region.sh "Cyprus" "Europe/Cyprus"
#./download_region.sh "North Macedonia" "Europe/North_Macedonia"
#./download_region.sh "Albania" "Europe/Albania"
#./download_region.sh "Serbia" "Europe/Serbia"
#./download_region.sh "Bosnia and Herzegovina" "Europe/Bosnia_and_Herzegovina"
#./download_region.sh "Croatia" "Europe/Croatia"
#./download_region.sh "Slovenia" "Europe/Slovenia"
#./download_region.sh "Russia" "Europe/Russia"
#./download_region.sh "Georgia" "Europe/Georgia"
#./download_region.sh "Montenegro" "Europe/Montenegro"
#./download_region.sh "Iceland" "Europe/Iceland"
#./download_region.sh "Guernsey" "Europe/Guernsey"
#./download_region.sh "Kosovo" "Europe/Kosovo"
#./download_region.sh "Armenia" "Europe/Armenia"

./download_region.sh "Africa" "Africa"

#./download_region.sh "Algeria" "Africa/Algeria"
#./download_region.sh "Egypt" "Africa/Egypt"
#./download_region.sh "Libya" "Africa/Libya"
#./download_region.sh "Morocco" "Africa/Morocco"
#./download_region.sh "South Africa" "Africa/South_Africa"
#./download_region.sh "Tunisia" "Africa/Tunisia"
#./download_region.sh "Namibia" "Africa/Namibia"
#./download_region.sh "Botswana" "Africa/Botswana"
#./download_region.sh "eSwatini" "Africa/eSwatini"
#./download_region.sh "Lesotho" "Africa/Lesotho"
#./download_region.sh "Nigeria" "Africa/Nigeria"
#./download_region.sh "Mozambique" "Africa/Mozambique"
#./download_region.sh "Kenya" "Africa/Kenya"
#./download_region.sh "Uganda" "Africa/Uganda"
#./download_region.sh "Ghana" "Africa/Ghana"
#./download_region.sh "Zimbabwe" "Africa/Zimbabwe"
#./download_region.sh "Angola" "Africa/Angola"
#./download_region.sh "Tanzania" "Africa/Tanzania"
#./download_region.sh "Burundi" "Africa/Burundi"
#./download_region.sh "Malawi" "Africa/Malawi"
#./download_region.sh "Ivory Coast" "Africa/Ivory_Coast"
#./download_region.sh "Rwanda" "Africa/Rwanda"
#./download_region.sh "Réunion" "Africa/Réunion"
#./download_region.sh "Senegal" "Africa/Senegal"
#./download_region.sh "Mali" "Africa/Mali"
#./download_region.sh "Niger" "Africa/Niger"
#./download_region.sh "Benin" "Africa/Benin"
#./download_region.sh "Togo" "Africa/Togo"
#./download_region.sh "Guinea" "Africa/Guinea"
#./download_region.sh "Saint Helena" "Africa/Saint_Helena"
#./download_region.sh "Cameroon" "Africa/Cameroon"
#./download_region.sh "Cape Verde" "Africa/Cape_Verde"
#./download_region.sh "Central African Republic" "Africa/Central_African_Republic"
#./download_region.sh "Chad" "Africa/Chad"
#./download_region.sh "Comoros" "Africa/Comoros"
#./download_region.sh "Congo (Republic)" "Africa/Congo_(Republic)"
#./download_region.sh "Congo (Zaire)" "Africa/Congo_(Zaire)"
#./download_region.sh "Djibouti" "Africa/Djibouti"
#./download_region.sh "Equatorial Guinea" "Africa/Equatorial_Guinea"
#./download_region.sh "Eritrea" "Africa/Eritrea"
#./download_region.sh "Ethiopia" "Africa/Ethiopia"
#./download_region.sh "Gabon" "Africa/Gabon"
#./download_region.sh "Gambia" "Africa/Gambia"
#./download_region.sh "Guinea-Bissau" "Africa/Guinea-Bissau"
#./download_region.sh "Liberia" "Africa/Liberia"
#./download_region.sh "Madagascar" "Africa/Madagascar"
#./download_region.sh "Mauritania" "Africa/Mauritania"
#./download_region.sh "Mauritius" "Africa/Mauritius"
#./download_region.sh "Mayotte" "Africa/Mayotte"
#./download_region.sh "Seychelles" "Africa/Seychelles"
#./download_region.sh "Sierra Leone" "Africa/Sierra_Leone"
#./download_region.sh "Somalia" "Africa/Somalia"
#./download_region.sh "Zambia" "Africa/Zambia"
#./download_region.sh "Sao Tome and Principe" "Africa/Sao_Tome_and_Principe"
#./download_region.sh "Burkina Faso" "Africa/Burkina_Faso"

./download_region.sh "Asia" "Asia"

#./download_region.sh "Azerbaijan" "Asia/Azerbaijan"
#./download_region.sh "Bahrain" "Asia/Bahrain"
#./download_region.sh "Hong Kong and Macau" "Asia/Hong_Kong_and_Macau"
#./download_region.sh "Kuwait" "Asia/Kuwait"
#./download_region.sh "Malaysia" "Asia/Malaysia"
#./download_region.sh "Oman" "Asia/Oman"
#./download_region.sh "Qatar" "Asia/Qatar"
#./download_region.sh "Saudi Arabia" "Asia/Saudi_Arabia"
#./download_region.sh "Singapore" "Asia/Singapore"
#./download_region.sh "Taiwan" "Asia/Taiwan"
#./download_region.sh "Thailand" "Asia/Thailand"
#./download_region.sh "United Arab Emirates" "Asia/United_Arab_Emirates"
#./download_region.sh "India" "Asia/India"
#./download_region.sh "Indonesia" "Asia/Indonesia"
#./download_region.sh "Jordan" "Asia/Jordan"
#./download_region.sh "Vietnam" "Asia/Vietnam"
#./download_region.sh "Philippines" "Asia/Philippines"
#./download_region.sh "Lebanon" "Asia/Lebanon"
#./download_region.sh "Kazakhstan" "Asia/Kazakhstan"
#./download_region.sh "Yemen" "Asia/Yemen"
#./download_region.sh "Brunei" "Asia/Brunei"
#./download_region.sh "Sri Lanka" "Asia/Sri_Lanka"
#./download_region.sh "Maldives" "Asia/Maldives"
#./download_region.sh "Nepal" "Asia/Nepal"
#./download_region.sh "Bangladesh" "Asia/Bangladesh"
#./download_region.sh "Israel" "Asia/Israel"
#./download_region.sh "Iraq" "Asia/Iraq"
#./download_region.sh "Cambodia" "Asia/Cambodia"

./download_region.sh "Australia/Oceania" "Australia_Oceania"

#./download_region.sh "Australia" "Australia_Oceania/Australia"
#./download_region.sh "New Zealand" "Australia_Oceania/New_Zealand"
#./download_region.sh "Fiji" "Australia_Oceania/Fiji"

# Requests

# USA & France
./download_region.sh "USA,France" "USA_and_France"

./download_region.sh "\
Austria,\
Germany,\
Italy,\
Liechtenstein,\
Luxembourg,\
Slovenia,\
Switzerland" \
"Europe_Austria_Germany_Italy,Liechtenstein_Luxembourg_Slovenia_Switzerland"


# UK & Ireland
./download_region.sh "Ireland,United Kingdom" "UK_and_Ireland"


# Switzerland Surrounding
bash ./download_region.sh "\
Albania,\
Croatia,\
France,\
Germany,\
Italy,\
Switzerland" \
"Switzerland_Surrounding"

# Romania, Moldova, Ukraine
bash ./download_region.sh "\
Romania,Moldova,Ukraine\
" "Romania_Moldova_Ukraine"

# South East Asia
bash ./download_region.sh "\
Singapore,\
Malaysia,\
Thailand" \
"Singapore_Malaysia_Thailand"

# USA and Canada
bash ./download_region.sh "\
USA,\
Canada" \
"Canada_and_USA"

# USA, Mexico and Canada
bash ./download_region.sh "\
USA,\
Mexico,\
Canada" \
"Canada_Mexico_and_USA"
