import android.app.Instrumentation;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Parcel;
import android.support.test.InstrumentationRegistry;
import android.support.test.filters.MediumTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.SmallTest;
import android.util.Log;
import android.util.Pair;
import android.widget.Toast;

import com.here.android.mpa.common.ApplicationContext;
import com.here.android.mpa.common.MapEngine;
import com.here.android.mpa.common.OnEngineInitListener;
import com.here.android.mpa.odml.MapLoader;
import com.here.android.mpa.odml.MapPackage;
import com.navdy.mapdownloader.MainActivity;
//import com.navdy.mapdownloader.MapListView;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.FileUtils;

@MediumTest
@RunWith(AndroidJUnit4.class)
public class Download {
    private static final String TAG = "Download";
    private MapLoader m_mapLoader;

    final Object syncObject = new Object();

    private String mapsPath = "";
    private String region = "";

    @Rule
    public ActivityTestRule<MainActivity> rule  = new  ActivityTestRule<>(MainActivity.class);

//    @Test
//    public void Australia() throws InterruptedException {
//        String mapsPath = "/sdcard/Australia/.here-maps";
//        region = "Australia/Oceania";
//        DownloadMaps(mapsPath);
//    }
//
//    @Test
//    public void Asia() throws InterruptedException {
//        String mapsPath = "/sdcard/Asia/.here-maps";
//        region = "Asia";
//        DownloadMaps(mapsPath);
//    }

    @Test
    public void DownloadMaps() throws InterruptedException {

        Bundle extras = InstrumentationRegistry.getArguments();
        if ( extras != null ) {
            if ( extras.containsKey ( "mapsPath" ) ) {
                mapsPath = extras.getString("mapsPath");
            }
            if ( extras.containsKey ( "region" ) ) {
                region = extras.getString("region");
            }
        }
        mapsPath = System.getProperty("mapsPath", mapsPath);
        region = System.getProperty("region", region);

        MainActivity m_activity = rule.getActivity();

        String intentName = "";

        if ("".equals(mapsPath)) {
            TestLog("No mapsPath, please run gradle with \"-DmapsPath=/sdcard/my_maps\" or similar");
            mapsPath = "/sdcard";
        }


        if (! mapsPath.endsWith("/.here-maps")) {
            mapsPath = mapsPath.concat("/.here-maps").replace("//", "/");
        }

        try {
            ApplicationInfo ai = m_activity.getPackageManager().getApplicationInfo(m_activity.getPackageName(), PackageManager.GET_META_DATA);
            Bundle bundle = ai.metaData;
            intentName = bundle.getString("INTENT_NAME");
        } catch (PackageManager.NameNotFoundException e) {
            TestLog("Failed to find intent name, NameNotFound: " + e.getMessage());
        }
        boolean success = com.here.android.mpa.common.MapSettings.setIsolatedDiskCacheRootPath(mapsPath, intentName);
        assert success;

        File p = new File(mapsPath);
        if (!p.exists()) {
            if (!p.mkdirs()) {
                // Could not create folder, we don't have required permissions.
                // Abort now, else the sdk function below will exit the app
                TestLog( "Can't write to : " + mapsPath);
                assert false;
                return;
            }
        }
        TestLog( "Storing maps in: " + mapsPath);

        MapEngine.getInstance().init(new ApplicationContext(m_activity), new OnEngineInitListener() {
            @Override
            public void onEngineInitializationCompleted(Error error) {
                if (error == Error.NONE) {
                    /*
                     * Similar to other HERE Android SDK objects, the MapLoader can only be
                     * instantiated after the MapEngine has been initialized successfully.
                     */
                    getMapPackages();
                } else {
                    TestLog( "Failed to initialize MapEngine: " + error);
                }
            }
        });

        synchronized (syncObject){
            syncObject.wait();
        }

        m_activity.stopService(new Intent(m_activity.getBaseContext(),
                com.here.android.mpa.service.MapService.class));
    }

    private void TestLog(String str) {
        Log.i(TAG, str);
        SimpleDateFormat s = new SimpleDateFormat("hh:mm:ss > ");
        String timstamp = s.format(new Date());

        Bundle b = new Bundle();
        b.putString(Instrumentation.REPORT_KEY_STREAMRESULT, "\n" + timstamp + str);
        InstrumentationRegistry.getInstrumentation().sendStatus(0, b);
    }

    protected void getMapPackages() {
        m_mapLoader = MapLoader.getInstance();
        // Add a MapLoader listener to monitor its status
        m_mapLoader.addListener(m_listener);
        m_mapLoader.getMapPackages();
    }

    protected void downloadMapPackage(List<MapPackage> root) {
        List<Integer> idList = new ArrayList<>();
        List<String> regionList = new ArrayList<>();
        try {
            regionList = Arrays.asList(region.split(","));
        } catch (NullPointerException ex) {

        }
        if ("".equals(region)){
            TestLog("Available Regions:\n");
        }
        for (MapPackage mapPackage: root) {
            if ("".equals(region)){
                String title = mapPackage.getEnglishTitle();
                String path = title.replaceAll("[/\\ ]", "_");
                TestLog("# \""+ title +"\" \"" + path + "\"");
                for (MapPackage subPackage: mapPackage.getChildren()) {
                    title = subPackage.getEnglishTitle();
                    path = mapPackage.getEnglishTitle().replaceAll("[/ \\\\]", "_") + "/" + title.replaceAll("[/ \\\\]", "_");
                    TestLog("# \""+ title +"\" \"" + path + "\"");
                }
            }
            if (regionList.contains(mapPackage.getEnglishTitle())) {
                // Download
                TestLog("downloading " + mapPackage.getEnglishTitle());
                idList.add(mapPackage.getId());

            } else {
                for (MapPackage subPackage: mapPackage.getChildren()) {
                    if (regionList.contains(subPackage.getEnglishTitle())) {
                        // Download
                        TestLog("downloading " + subPackage.getEnglishTitle());
                        idList.add(subPackage.getId());
                    }
                }
            }
        }
        if (!idList.isEmpty()) {
            Boolean success = m_mapLoader.installMapPackages(idList);
            assert success;
        } else if ("".equals(region)){
            TestLog("\nPlease set download region with -Dregion=\"Australia/Oceania\" or similar");
            synchronized (syncObject){
                syncObject.notify();
            }
            assert false;
        }
    }

    // Listener to monitor all activities of MapLoader.
    private MapLoader.Listener m_listener = new MapLoader.Listener() {
        @Override
        public void onProgress(int i) {
            TestLog( "onProgress:" + i);
        }

        @Override
        public void onInstallationSize(long diskSize, long networkSize) {
            TestLog("onInstallationSize:" + diskSize);
        }

        @Override
        public void onGetMapPackagesComplete(MapPackage rootMapPackage,
                                             MapLoader.ResultCode resultCode) {
            TestLog("onGetMapPackagesComplete: " + resultCode.toString());
            /*
             * Please note that to get the latest MapPackage status, the application should always
             * use the rootMapPackage that being returned here. The same applies to other listener
             * call backs.
             */
            if (resultCode == MapLoader.ResultCode.OPERATION_SUCCESSFUL) {
                List<MapPackage> children = rootMapPackage.getChildren();
                downloadMapPackage(children);

            } else if (resultCode == MapLoader.ResultCode.OPERATION_BUSY) {
                // The map loader is still busy, just try again.
                m_mapLoader.getMapPackages();
            }
        }

        @Override
        public void onCheckForUpdateComplete(boolean updateAvailable, String current, String update,
                                             MapLoader.ResultCode resultCode) {
            Log.d(TAG, "onCheckForUpdateComplete: " + current);

            if (resultCode == MapLoader.ResultCode.OPERATION_BUSY) {
                // The map loader is still busy, just try again.
                m_mapLoader.checkForMapDataUpdate();

            } else  {

                File versfile = new File(mapsPath + "/version");
                try {
                    FileUtils.writeStringToFile(versfile, current);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                synchronized (syncObject) {
                    syncObject.notify();
                }
            }

        }

        @Override
        public void onPerformMapDataUpdateComplete(MapPackage rootMapPackage,
                                                   MapLoader.ResultCode resultCode) {
            Log.d(TAG, "onPerformMapDataUpdateComplete: " + resultCode.toString());
        }

        @Override
        public void onInstallMapPackagesComplete(MapPackage rootMapPackage,
                                                 MapLoader.ResultCode resultCode) {
            TestLog("onInstallMapPackagesComplete: " + resultCode.toString());
            m_mapLoader.checkForMapDataUpdate();

        }

        @Override
        public void onUninstallMapPackagesComplete(MapPackage rootMapPackage,
                                                   MapLoader.ResultCode resultCode) {
            TestLog("onUninstallMapPackagesComplete: " + resultCode.toString());
        }
    };


}
