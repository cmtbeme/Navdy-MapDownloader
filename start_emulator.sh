echo "Creating emulator profile"
avdmanager create avd -f -n Nexus_5_5.1 -b default/x86 -c 20000M -k "system-images;android-22;default;x86" -d Nexus\ 5 -p /root/android_emulator

echo "Starting emulator"
screen -dmS emulator ~/emulator/emulator @Nexus_5_5.1 -gpu guest -verbose -wipe-data -no-window

function wait_emulator_to_be_ready () {
  boot_completed=false
  while [ "$boot_completed" == false ]; do
    status=$(adb wait-for-device shell getprop sys.boot_completed | tr -d '\r')
    echo "Boot Status: $status"

    if [ "$status" == "1" ]; then
      boot_completed=true
    else
      sleep 3
    fi
  done;
}

echo "Wait for emulator to be ready"
wait_emulator_to_be_ready
echo "Emulator is ready"
